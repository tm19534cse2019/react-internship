import { useState } from "react";

const useCartCounter = (initialValue) => {
    const [value, setValue] = useState(initialValue);
    function changeHandler(event) {
        setValue(parseInt(event.target.value));
    }
    function increment() {
        setValue((prev) => prev + 1);
    }
    function decrement() {
        setValue((prev) => prev - 1);
    }
    function reset() {
        setValue(initialValue);
    }
    return { value, changeHandler, increment, decrement, reset };
};

export default useCartCounter;
