import React from "react";
import useCartCounter from "../hooks/useCartCounter";

function B() {
    const { value, changeHandler, increment, decrement, reset } =
        useCartCounter(10);
    return (
        <div>
            <h1>Cart</h1>
            <p>{value}</p>
            <input type="number" value={value} onChange={changeHandler} />
            <div className="btn-grp">
                <button onClick={decrement}>-</button>
                <button onClick={reset}>Reset</button>
                <button onClick={increment}>+</button>
            </div>
        </div>
    );
}

export default B;
